/**
 * rlocker - A Shiny xAPI Statement Generator & Storage Mechanism
 * @author Bob Carey (https://bobcarey.me).
 * @license ISC license
 * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
 **/
"use strict";

/* @namespace: rlocker */
var rlocker = rlocker || {};

/* @config */
Shiny.addCustomMessageHandler('rlocker-setup', function(config) {
  rlocker.config = config;
});

/**
 * This class requires the following modules:
 * {@link https://github.com/adlnet/xAPIWrapper}
 * {@link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136}
 * @requires xapiwrapper/adl
 * @requires uuid/generator
 **/

/* Check that requirements are met. */
if (typeof ADL == "undefined") {
  throw "Error: ADL Wrapper not defined";
} else if (typeof UUID == "undefined") {
  throw "Error: UUID Generator not defined";
}

/**
 * Creates a new Learning Locker instance.
 * @class
 */
rlocker = (function (config) {

  function Locker() {
    this.debug = true,
    this.config = config ? config : {
      endpoint: 'http://localhost:8000/xapi/',
      auth: 'Basic ' + toBase64('abcd:1234')
    },
    this.session = {
      id: null,
      launched: null
    },
    this.agent = null,
    this.activity = null
  }

  let locker = new Locker();

  locker.init = () => {
    ADL.XAPIWrapper.changeConfig(locker.config);

    locker.setSession();
    locker.setCurrentAgent("mailto:default@example.org");
    locker.setCurrentActivity(window.location.href, document.title);

    locker.experienced_xAPI();
  };

  locker.setSession = () => {
    if (typeof Storage !== "undefined") {
      let sid = sessionStorage.getItem("sid");

      if (!sid) {
        /* generate universally unique identifier */
        sid = UUID.generate();
        sessionStorage.setItem("sid", sid);
        locker.session.launched = true;
      } else {
        locker.session.launched = false;
      }
      locker.session.id = sid;
    } else {
      locker.session.id = "0000-0000-0000-0000";
    }
  };

  locker.store = statement => {
    if (locker.debug) {
      console.info(statement);
      ADL.XAPIWrapper.sendStatement(statement, function (request, response) {
        if (request.status != 200) {
          console.error(request);
        }
        console.info(response);
      });
    } else {
      ADL.XAPIWrapper.sendStatement(statement);
    }
  };

  locker.setCurrentAgent = (mbox, name = locker.session.id) => {
    locker.agent = new ADL.XAPIStatement.Agent(
      ADL.XAPIWrapper.hash(mbox),
      name
    );
  };

  locker.getCurrentAgent = () => {
    return locker.agent;
  };

  locker.setCurrentActivity = (href, title) => {
    locker.activity = new ADL.XAPIStatement.Activity(href, title);
  };

  locker.getCurrentActivity = () => {
    return locker.activity;
  };

  locker.getVerb = (ref, verb) => {
    return new ADL.XAPIStatement.Verb(ref, verb);
  };

  locker.createBasicStatement = verb => {
    let statement = new ADL.XAPIStatement(
      locker.getCurrentAgent(),
      locker.getVerb("http://adlnet.gov/expapi/verbs/" + verb, verb),
      locker.getCurrentActivity()
    );
    return statement;
  };

  locker.experienced_xAPI = () => {
    let verb = locker.session.launched ? "launched" : "experienced";
    locker.store(locker.createBasicStatement(verb));

    locker.session.launched = false;
  };

  locker.terminated_xAPI = () => {
    locker.store(locker.createBasicStatement("terminated"));
  };

  locker.completed_xAPI = () => {
    locker.store(locker.createBasicStatement("completed"));
  };

  locker.answered_xAPI = (question, data, answered, success) => {
    let attempt = data["attempt"] ? data["attempt"] : 1;
    let location = locker.activity.id + "#" + question;
    let title = locker.activity.definition.name['en-US'] + " :: " + question;
    let agent = locker.getCurrentAgent();
    let verb = locker.getVerb(
      "http://adlnet.gov/expapi/verbs/answered",
      "answered"
    );
    let activity = new ADL.XAPIStatement.Activity(location, title);
    let statement = new ADL.XAPIStatement(agent, verb, activity);

    statement.object.definition.type =
      "http://adlnet.gov/expapi/activities/interaction";
    statement.object.definition.interactionType = data["interactionType"];
    statement.object.definition.correctResponsesPattern = [
      JSON.stringify(data["validateOn"]),
      data["answers"].toString()
    ];

    statement.result = {
      success: success,
      response: String(answered),
      extensions: {
        "http://adlnet.gov/expapi/verbs/attempted": attempt.toString()
      }
    };

    locker.store(statement);

    return statement;
  };

  locker.interacted_xAPI = element => {
    let location = locker.activity.id + "#" + element;
    let title = locker.activity.definition.name['en-US'] + " :: " + element;
    let agent = locker.getCurrentAgent();
    let verb = locker.getVerb(
      "http://adlnet.gov/expapi/verbs/interacted",
      "interacted"
    );
    let activity = new ADL.XAPIStatement.Activity(location, title);
    let statement = new ADL.XAPIStatement(agent, verb, activity);

    statement.object.definition.type =
      "http://adlnet.gov/expapi/activities/interaction";
    statement.object.definition.interactionType = "other";

    locker.store(statement);
  };

  locker.init();

  return locker;
})(rlocker.config);
