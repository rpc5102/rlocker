library(shiny)
library(rlocker)

shinyApp(
  ui = fluidPage(
    tags$head(
      # Attach Locker
      setupLocker(
        list(
          endpoint = "https://learning-locker.bobcarey.me/data/xAPI/",
          auth = "Basic MDVhZTMxZTIwNDhlMjFlYWIyNjQ3NjRlMWVmM2RjNzMxYmJlYjU3ZTpjY2NkNzcwZDM1NzY5ZGE0OWE2NDRiNDhkMjNiZmE0ZWYxYjczNzdm"
        )
      )
    ),

    # Application title
    titlePanel("rlocker demo"),

    fluidRow(
      column(12,
        mainPanel(
          textOutput("config")
        )
      )
    )
  ),
  server = function(input, output, session) {
    rlocker::observeEvents(session)
  }
)
