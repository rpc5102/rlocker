\docType{package}
\name{learningLocker}
\alias{rlocker}
\alias{rlocker-package}
\title{Shiny + Learning Locker}
\usage{
learningLocker(token)
}
\arguments{
\item{token}{Learning Locker API token}
}
\description{
Prints 'Hello, world!'.
}
\examples{
learningLocker("1234")
}
